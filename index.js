const http = require('http')
const request = require('request');

const port = 7070
const firebaseAdminKey = ''
const appId = 'io.fluffypix.app'
const channelId = "io.fluffypix.app.push"

const tryRequestListener = function (req, res) {
    try {
        requestListener(req, res)
    } catch (_) {
        res.writeHead(500, { "Content-Type": "application/json" })
        res.write(JSON.stringify({ "error": "Internal server error" }))
        res.end()
        return
    }
}

const requestListener = function (req, res) {
    if (req.method != 'POST') {
        res.writeHead(404, { "Content-Type": "application/json" })
        res.write(JSON.stringify({ "error": "Not found" }))
        res.end()
        return
    }

    var requestData

    req.on('data', function (data) {
        try {
            requestData = String.fromCharCode(data)
        } catch (_) { requestData = null }
    })

    req.on('end', function () {
        console.log('[NEW REQUEST] ' + req.url)
        console.log('[REQUEST DATA]')
        console.log(requestData)
        const urlParts = req.url.split("/")
        if (urlParts.length < 4) {
            console.log('[ERROR]: URL has not enough parameters')
            res.writeHead(400, { "Content-Type": "application/json" })
            res.write(JSON.stringify({ "error": 'Bad request' }))
            res.end()
            return
        }
        const token = urlParts[urlParts.length - 1]
        const platform = urlParts[urlParts.length - 2]
        if (!token || !platform) {
            console.log('[ERROR]: URL has not enough parameters')
            res.writeHead(400, { "Content-Type": "application/json" })
            res.write(JSON.stringify({ "error": 'Bad request' }))
            res.end()
            return
        }
        const salt = req.headers['encryption']
        const cryptoKey = req.headers['crypto-key']
        const contentLength = req.headers['content-length']
        const contentType = req.headers['content-type']
        const contentEncoding = req.headers['content-encoding']
        if (!salt || !cryptoKey || !contentLength || !contentType || !contentEncoding) {
            console.log('[ERROR]: Missing encryption headers')
            console.log(req.headers)
            res.writeHead(400, { "Content-Type": "application/json" })
            res.write(JSON.stringify({ "error": 'Bad request' }))
            res.end()
            return
        }

        const notification = {
            "appid": appId,
            "to": token,
            "notification": {
                "title": "New activities in FluffyPix",
                "body": "Open the app to see more details",
                "sound": "default",
                "icon": "notifications_icon",
                "badge": 1,
                "tag": channelId,
                "android_channel_id": channelId
            },
            "android": {
                "priority": "high"
            },
            "data": {
                "click_action": "FLUTTER_NOTIFICATION_CLICK",
                "ciphertext": requestData,
                "salt": salt,
                "crypto_key": cryptoKey,
                "content_length": contentLength,
                "content_type": contentType,
                "content_encoding": contentEncoding
            }
        };
        console.log('[SENDING]');
        console.log(notification)
        request.post("https://fcm.googleapis.com/fcm/send", {
            json: notification,
            headers: {
                "Authorization": "key=" + firebaseAdminKey,
                "Content-Type": "application/json"
            }
        }, function (err, result, body) {
            console.log('[SUCCESS]: ' + body)
            res.writeHead(200, { "Content-Type": "application/json" })
            res.write('{}')
            res.end()
            return
        });
    })
}

const server = http.createServer(tryRequestListener)
server.listen(port)
console.log("FluffyPix Push Gateway now listens to port " + port)